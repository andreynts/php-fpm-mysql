# Docker + PHP + Nginx + MySQL + RabbitMQ + Composer
Docker Compose configuration to run PHP 7.3 with Nginx, PHP-FPM, 
MySQL 5.7, RabbitMQ 3.7.17 and Composer.

## Overview

This Docker Compose configuration lets you run easily PHP 7.3 with Nginx, 
PHP-FPM, MySQL 5.7, RabbitMQ 3.7.17 and Composer.
It exposes 5 services:

* web (Nginx)
* php (PHP 7.3 with PHP-FPM)
* db (MySQL 5.7)
* rabbitmq (RabbitMQ 3.7.17)
* composer

The PHP image comes with the most commonly used extensions and is configured with xdebug.
Nginx default configuration is set up for Symfony 4 (but can be easily changed) and will serve your working directory.
Composer is run at boot time and will automatically install the vendors.

## Install prerequisites

For now the project has been tested on Linux only but should run fine on Docker for Windows and Docker for Mac.

You will need:

* [Docker CE](https://docs.docker.com/engine/installation/)
* [Docker Compose](https://docs.docker.com/compose/install)
* Git (optional)

## How to use it

### Starting Docker Compose

Checkout the repository or download the sources.

Simply run `docker-compose up` and you are done.

Nginx will be available on `localhost:80` and MySQL on `localhost:3306`.

You can access to RabbitMQ GUI from your browser 
via `http://{network-gateway-ip or container-ip}:15672` with `rabbitmq:rabbitmq`.

### Using Composer

`docker-compose run composer <cmd>`

Where `cmd` is any of the available composer command.

### Using MySQL

Default connection:

`docker-compose exec db mysql -u root -p`

Using .env file default parameters:

`docker-compose exec db mysql -u root -p docker_db`

If you want to connect to the DB from another container (from the `php` one for instance), the host will be the service name: `db`.

### Using PHP

You can execute any command on the `php` container as you would do on any docker-compose container:

`docker-compose exec php php -v`

## Change configuration

### Configuring PHP

To change PHP's configuration edit `.docker/conf/php/php.ini`.
Same goes for `.docker/conf/php/xdebug.ini`.

You can add any .ini file in this directory, don't forget to map them by adding a new line in the php's `volume` section of the `docker-compose.yml` file.

### Configuring MySQL

If you want to change the db name, db user and db password simply edit the `.env` file at the project's root.

### Configuring RabbitMQ

If you want to change the `user`, `password` and `vhost` simply edit the `.env` file at the project's root.
